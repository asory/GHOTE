class PagesController <ApplicationController
 
  def index
   
   @rooms=Room
   @rooms_count = Room.group(:estatus).count 
    @pastel=Gchart.pie( 
                    :size   => '600x400',
                    :title  => "Estado de Habitaciones",
                    :legend => ['Ocupadas', 'Disponibles', 'En Servicio', 'Fuera de Servicio'],
                    :custom => "chco=FFF110,FF0000",
                    :data   => [120, 45, 25, 55, 20])
    
    
  end
end
