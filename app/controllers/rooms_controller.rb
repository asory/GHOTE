class RoomsController <ApplicationController
  before_action :authenticate_user!                                                                 
  before_action :set_room, only: [:show, :edit, :update, :destroy]
   

  # GET /rooms
  # GET /rooms.json
  def index
    @RoomTypes = RoomType.all
    if params[:search]
    @rooms = Room.search(params[:search]).order("created_at DESC")
   else
    @rooms = Room.all.order('created_at DESC')
    end
    
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
     @RoomTypes = RoomType.all
  end

  # GET /rooms/new
  def new
    @room = Room.new
    @RoomTypes = RoomType.all
  end

  # GET /rooms/1/edit
  def edit
    @room=Room.find(params[:id])
    @RoomTypes = RoomType.all
  end

  # POST /rooms
  # POST /rooms.json
  def create
  
   @room = Room.new(room_params)
   @RoomType= RoomType.find(@room.room_type_id )
      
    respond_to do |format|
      if @room.save
        format.html { redirect_to @room, notice: 'Habitacion registrada exitosamente.'}
        format.json { render :show, status: :created, location: @room }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        unless index 
        format.html { redirect_to @room, notice: 'Habitacion actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room.destroy
    respond_to do |format|
      format.html { redirect_to rooms_url, notice: 'Habitacion eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:cod_hab, :piso, :estatus,:room_type_id)
    end
  
end
