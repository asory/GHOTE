class BillsController <ApplicationController
  before_filter :authenticate_user!
  before_action :set_bill, only: [:show, :edit, :update, :destroy]
 

  # GET /bills
  # GET /bills.json
  def index
   @bills = Bill.all 
   @bill_details =BillDetail.all
   @reserve=Reserve.all
  end

  # GET /bills/1
  # GET /bills/1.json
  def show
   @bill = Bill.find(params[:id])
   @client= Client.all
     @reserve=Reserve.all
     @details=BillDetail.all
     @expends=Expend.all
   respond_to do |format|
      format.html
      format.pdf { render :pdf => generate_pdf(@bill) }
    end
  end

  # GET /bills/new
  def new
    @bill = Bill.new
    @client = Client.where(cedula: @bill.client_id).last
    @bill.codigo= "0000205"+""+@bill.id.to_s 
    @clients= Client.all
    @reserves=Reserve.all
   # @bill_details=
    @bill.bill_details.build#.build_expend
    #@bill.expends.build
  #  @expends=@bill_details.expend.build
     if params[:search]
          @search = Client.search(params[:search])
     end
  end

  # GET /bills/1/edit
  def edit
     @bill = Bill.find(params[:id])
    @client= Client.all
     @reserve=Reserve.all
     @details=BillDetail.all
     @expends=Expend.all
  end

  # POST /bills
  # POST /bills.json
  def create
    @bill = Bill.new(bill_params)
   # @client = Client.find(@bill.client_id)
    #@reserf= Reserve.find(@bill.reserve_id)
    
    respond_to do |format|
      if @bill.save
        format.html { redirect_to @bill, notice: 'Factura  registrada exitosamente.' }
        format.json { render :show, status: :created, location: @bill }
      else
        format.html { render :new }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bills/1
  # PATCH/PUT /bills/1.json
  def update
    respond_to do |format|
      if @bill.update(bill_params)
        format.html { redirect_to @bill, notice: 'Factura actualizada correctamente.' }
        format.json { render :show, status: :ok, location: @bill }
      else
        format.html { render :edit }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bills/1
  # DELETE /bills/1.json
  def destroy
    @bill.destroy
    respond_to do |format|
      format.html { redirect_to bills_url, notice: 'Factura eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bill
      @bill = Bill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_params
      params.require(:bill).permit(:id, :client_id, :reserve_id,:monto_total,:codigo,:tipo_pago, 
      bill_detail_attributes: [:id, :cantidad, 
      expend_attributes:[:id, :descripcion] ])
    end
end
