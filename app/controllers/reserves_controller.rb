class ReservesController <ApplicationController
  before_action :authenticate_user!                                                                 
  before_action :set_reserf, only: [:show, :edit, :update, :destroy]

  # GET /reserves
  # GET /reserves.json
  def index
    if params[:search]
    @reserves = Reserve.search(params[:search]).order("created_at DESC")
   else
    @reserves = Reserve.all.order('created_at DESC')
    end
  end

  # GET /reserves/1
  # GET /reserves/1.json
  def show
     @rooms = Room.all
  end

  # GET /reserves/new
  def new
    @reserf = Reserve.new
    @clients = Client.buscar(params[:search])
    @rooms = Room.all
  end

  # GET /reserves/1/edit
  def edit
      @reserf = Reserve.find(params[:id])
      @clients = Client.all
      @rooms = Room.all
  end

  # POST /reserves
  # POST /reserves.json
  def create
    @reserf = Reserve.new(reserf_params)
    #@reserf.cal_monto


    respond_to do |format|
      if @reserf.save
        format.html { redirect_to @reserf, notice: 'Reserva registrada exitosamente.' }
        format.json { render :show, status: :created, location: @reserf }
      else
        format.html { render :new }
        format.json { render json: @reserf.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reserves/1
  # PATCH/PUT /reserves/1.json
  def update
    respond_to do |format|
      if @reserf.update(reserf_params)
        format.html { redirect_to @reserf, notice: 'Reserva actualizada correctamente.' }
        format.json { render :show, status: :ok, location: @reserf }
      else
        format.html { render :edit }
        format.json { render json: @reserf.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reserves/1
  # DELETE /reserves/1.json
  def destroy
    @reserf.destroy
    respond_to do |format|
      format.html { redirect_to reserves_url, notice: 'Reserva eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reserf
      @reserf = Reserve.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reserf_params
      params.require(:reserve).permit(:fecha_init,:fecha_fin, :estatus, :monto, :client_id, :room_id)
    end
    
end    

