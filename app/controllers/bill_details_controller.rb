class BillDetailsController <ApplicationController
  before_action :authenticate_user!                                                                 
  before_action :set_bill_detail, only: [:show, :edit, :update, :destroy]

  # GET /bill_details
  # GET /bill_details.json
  def index
    @bill_details = BillDetail.all
  end

  # GET /bill_details/1
  # GET /bill_details/1.json
  def show
  end

  # GET /bill_details/new
  def new
    @bill=Bill.find(params[:bill_id])
    @bill_detail = BillDetail.new
  end

  # GET /bill_details/1/edit
  def edit
    @bill = bill.find(params[:bill_id]);
   @bill_detail = bill_detail.find(params[:id]);
   @cantidad = @bill_detail.cantidad;
  end

  # POST /bill_details
  # POST /bill_details.json
  def create
    @bill.Bill.find(params[:bill_id])
    @bill_detail = @bill.BillDetails.new(bill_detail_params)
    redirect_to bill_patch(@bill)

    respond_to do |format|
      if @bill_detail.save
        format.html { redirect_to @bill, notice: 'Bill detail was successfully created.' }
        format.json { render :show, status: :created, location: @bill_detail }
      else
        format.html { render :new }
        format.json { render json: @bill_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bill_details/1
  # PATCH/PUT /bill_details/1.json
  def update
    @bill.Bill.find(params[:bill_id])
    @bill_detail = Bill_detail.find(params[:id]);
   @bill_detail.cantidad = @cantidad;

    respond_to do |format|
      if @bill_detail.update(bill_detail_params)
        format.html { redirect_to @bill, notice: 'Bill detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @bill_detail }
      else
        format.html { render :edit }
        format.json { render json: @bill_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bill_details/1
  # DELETE /bill_details/1.json
  def destroy
    @bill.Bill.find(params[:bill_id])
    @bill_detail = Bill_detail.find(params[:id]);
    if @bill_detail.destroy
    respond_to do |format|
      format.html { redirect_to @bill, notice: 'Bill detail was successfully destroyed.' }
      format.json { head :no_content }
       end
    else
      redirect_to @bill, :notice => "El comentario no se ha podido eliminar";
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bill_detail
      @bill_detail = BillDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_detail_params
      params.require(:bill_detail).permit(:expend_id, :bill_id, :cantidad)
    end
end
