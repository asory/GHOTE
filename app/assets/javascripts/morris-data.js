// Morris.js Charts sample data for SB Admin template

$(function() {

    // Area Chart
    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2014 Q1',
            sencilla: 669,
            doble: 450,
            suite: 247
        }, {
            period: '2014 Q2',
            sencilla: 778,
            doble: 594,
            suite: 241
        }, {
            period: '2014 Q3',
            sencilla: 712,
            doble: 769,
            suite: 301
        }, {
            period: '2014 Q4',
            sencilla: 767,
            doble: 597,
            suite: 89
        }, {
            period: '2015 Q1',
            sencilla: 810,
            doble: 914,
            suite: 293
        }, {
            period: '2015 Q2',
            sencilla: 670,
            doble: 293,
            suite: 181
        }, {
            period: '2015 Q3',
            sencilla: 820,
            doble: 795,
            suite: 588
        }, {
            period: '2015 Q4',
            sencilla: 150,
            doble: 967,
            suite: 175
        }, {
            period: '2016 Q1',
            sencilla: 687,
            doble: 460,
            suite: 128
        }, ],
        xkey: 'period',
        ykeys: ['sencilla', 'doble', 'suite'],
        labels: ['sencilla', 'doble', 'suite'],
        pointSize: 3,
        hideHover: 'auto',
        resize: true
    });

    // Donut Chart
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Canceladas",
            value: 12
        }, {
            label: "Activas",
            value: 03
        }, {
            label: "Vencidas",
            value: 2
        }],
        resize: true
    });

    // Line Chart
    Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'morris-line-chart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [{
            d: '2016-03-11',
            visitas: 802
        }, {
            d: '2016-03-12',
            visitas: 783
        }, {
            d: '2016-03-13',
            visiats: 820
        }, {
            d: '2016-03-17',
            visitas: 790
        }, {
            d: '2016-03-18',
            visitas: 1680
        }, {
            d: '2016-03-19',
            visitas: 1592
        }, {
            d: '2016-03-20',
            visitas: 1420
        }, {
            d: '2016-03-21',
            visitas: 882
        }, {
            d: '2016-03-22',
            visitas: 889
        }, {
            d: '2016-03-23',
            visitas: 819},
      ],
        // The name of the data record attribute that contains x-visitass.
        xkey: 'd',
        // A list of names of data record attributes that contain y-visitass.
        ykeys: ['visitas'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Visitas'],
        // Disables line smoothing
        smooth: false,
        resize: true
    });

    // Bar Chart
    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            consumo: 'Bebidas',
            cantidad: 136
        }, {
            consumo: 'Taxi',
            cantidad: 137
        }, {
            consumo: 'Tour',
            cantidad: 275
        }, {
            consumo: 'Servicio Hab ',
            cantidad: 380
       
        }],
        xkey: 'consumo',
        ykeys: ['cantidad'],
        labels: ['Cantidad'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });


});
