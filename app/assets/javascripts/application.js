// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require moment

//= require cocoon
//= javascript_include_tag :cocoon
//= require jquery.min
//= require morris.js
//= require morris.min.js
//= require raphael.min.js
//= require morris-data.js
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require bootstrap.min
//= require_tree .
//= require bootstrap-datepicker
//= 

//=$(document).on("focus", "[data-behaviour~='datepicker']", function(e){$(this).datepicker({"format": "yyyy-mm-dd", "weekStart": 1, "autoclose": true})


$("#room_status").focusout(function() {
    var restatus = $(this).val();
    $.ajax({
        url: "/rooms/<%= room.id %>/edit",
        type: "POST",
        data: { room_estatus: restatus}, // same way, other attributes
        success: function() {
            console.log("The result successfully came back from ther server.");
            // Now, here you can show a notice to the user that he has successfully update the record without reloading the page.
        }
    });
});
    
 