class Client < ActiveRecord::Base
 include Filterable
 has_many :reserves ,:dependent => :destroy
 validates  :cedula, uniqueness: true, presence: true
 validates  :telefono,  numericality: true, presence: true
 validates  :nombre, :apellido ,presence: true
 
 
 
  def self.search(search)
   
  where("apellido LIKE ?", "%#{search}%") 
  where("nombre LIKE ?", "%#{search}%") 
  
  end 
  
  def self.buscar(search)
   where("cedula LIKE ?", "%#{search}%") 
  end
end
