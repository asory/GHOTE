class RoomType < ActiveRecord::Base
   
    has_many :rooms
    validates :desc_th,:precio, presence: true
    validates :precio, numericality:{ :greater_than => 0}
    
    def mayus(desc_th)
    self.desc_th.capitalize

    end

end
