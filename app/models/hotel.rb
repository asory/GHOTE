class Hotel < ActiveRecord::Base
 include Filterable
 has_many :rooms
 validates  :rif, uniqueness: true, presence: true
 validates  :rif, :telefono,  numericality: true, presence: true
 validates  :tipo, :nombre, presence: true
   
end
