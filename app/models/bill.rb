class Bill < ActiveRecord::Base
 include Filterable
  belongs_to :client
  belongs_to :reserve
  
  has_many :bill_details, :dependent => :destroy
  has_many :expends, :through => :bill_details
  
  before_save :set_cod, :total_price

  accepts_nested_attributes_for :bill_details , allow_destroy: true, 
  reject_if: proc { |attr| attr[].blank? }
  
  validates :monto_total, presence: true , numericality: true
  validates :client_id, :reserve_id, presence: true
  
  
  def subtotal
   self.bill_details.cantidad * expend.monto 
  end
  
private

  def total_price
    @total_price ||= bill_details.includes(:expend).reduce(0) do |sum, b_det|
      sum + (b_det.cantidad * b_det.expend.monto)
    end
  end
  
  def set_cod
    self.codigo =  "f4004" + self.id.to_s 
  end

  def montototal
    self.monto_total = bill_details.collect { bill_details.cantidad * expend.monto }.sum
  end
  
  def total
  self.expends.sum(:price)
  end
end
     

