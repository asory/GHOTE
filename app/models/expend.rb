class Expend < ActiveRecord::Base
   has_many :bill_details
   has_many :bills, :through => :bill_details
   
   validates :descripcion, :monto, presence: true 
   validates :monto ,numericality:{ :greater_than => 0}
   
   def self.search(search)
       where("descripcion LIKE ?", "%#{search}%") 
   end
end