class Reserve < ActiveRecord::Base
 include Filterable

  belongs_to :client
  has_one :room
  before_save :cal_monto

  validates :fecha_fin, :fecha_init, :estatus ,:client_id , :room_id , presence: true
  #validates  :monto, numericality: true, presence: true
  validates_date :fecha_init, :on_or_after => lambda { Date.current }, message: "Solo a partir del dia de hoy "
  validates_date :fecha_fin, :after => :fecha_init 
  
  accepts_nested_attributes_for :client, :room 
 
 def cal_monto
 
   self.monto * (self.fecha_fin - (self.fecha_init))
 end
 
   private
  
  def self.search(search)
  where("estatus LIKE ?", "%#{search}%") 
  end

  
   
  def active
     hoy=Time.now
    if hoy>= fecha_init and hoy <= fecha_fin
     else 
       self.estatus==vencida
    end
  end
end
