class Room < ActiveRecord::Base
    include Filterable
    belongs_to :room_type
    has_many :reserves
    belongs_to :hotel
    
    
    validates :estatus,:piso, :cod_hab ,presence: true
    validates :cod_hab , uniqueness: true
    validates :piso, numericality: { only_integer: true }
    
    
    accepts_nested_attributes_for :room_type , reject_if: proc { |attr| attr[].blank? }
  
  
  def asig_cod_hab
    self[:cod_hab] = self.id
  end

  
  def self.search(search)
  where("estatus LIKE ?", "%#{search}%") 
  end
end
