class BillDetail < ActiveRecord::Base
  belongs_to :expend
  belongs_to :bill
  
  
  validates_presence_of :expend
  validates_presence_of :bill
  
  accepts_nested_attributes_for :expend
  
  
  def sub_total
  expend.monto * cantidad 
  end
  
end
