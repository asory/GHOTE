json.array!(@hotels) do |hotel|
  json.extract! hotel, :id, :rif, :nombre, :direccion, :ciudad, :estado, :telefono, :tipo
  json.url hotel_url(hotel, format: :json)
end
