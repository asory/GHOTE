json.array!(@bill_details) do |bill_detail|
  json.extract! bill_detail, :id, :expend_id, :bill_id, :cantidad
  json.url bill_detail_url(bill_detail, format: :json)
end
