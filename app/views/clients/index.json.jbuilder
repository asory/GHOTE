json.array!(@clients) do |client|
  json.extract! client, :id, :cedula, :nombre, :apellido, :edad, :direccion, :telefono
  json.url client_url(client, format: :json)
end
