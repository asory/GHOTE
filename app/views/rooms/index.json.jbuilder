json.array!(@rooms) do |room|
  json.extract! room, :id, :tipo, :piso, :estatus
  json.url room_url(room, format: :json)
end
