json.array!(@bills) do |bill|
  json.extract! bill, :id, :nro_reserva, :cliente, :references, :client_id, :reservation_id
  json.url bill_url(bill, format: :json)
end
