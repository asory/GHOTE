# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160327092502) do

  create_table "bill_details", force: :cascade do |t|
    t.integer  "expend_id"
    t.integer  "bill_id"
    t.integer  "cantidad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bill_details", ["bill_id"], name: "index_bill_details_on_bill_id"
  add_index "bill_details", ["expend_id"], name: "index_bill_details_on_expend_id"

  create_table "bills", force: :cascade do |t|
    t.integer  "client_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "monto_total"
    t.string   "codigo"
    t.integer  "reserve_id"
    t.string   "tipo_pago"
  end

  add_index "bills", ["client_id"], name: "index_bills_on_client_id"
  add_index "bills", ["reserve_id"], name: "index_bills_on_reserve_id"

  create_table "clients", force: :cascade do |t|
    t.string   "cedula"
    t.string   "nombre"
    t.string   "apellido"
    t.integer  "edad"
    t.string   "direccion"
    t.string   "telefono"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "expends", force: :cascade do |t|
    t.string   "descripcion"
    t.float    "monto"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "hotels", force: :cascade do |t|
    t.string   "rif"
    t.string   "nombre"
    t.string   "direccion"
    t.string   "telefono"
    t.string   "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "room_id"
  end

  add_index "hotels", ["room_id"], name: "index_hotels_on_room_id"
  add_index "hotels", ["user_id"], name: "index_hotels_on_user_id"

  create_table "reserves", force: :cascade do |t|
    t.date     "fecha_fin"
    t.date     "fecha_init"
    t.string   "estatus"
    t.float    "monto"
    t.integer  "client_id"
    t.integer  "room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "reserves", ["client_id"], name: "index_reserves_on_client_id"
  add_index "reserves", ["room_id"], name: "index_reserves_on_room_id"

  create_table "room_types", force: :cascade do |t|
    t.string   "desc_th"
    t.float    "precio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "cod_hab"
    t.string   "piso"
    t.string   "estatus"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "room_type_id"
  end

  add_index "rooms", ["room_type_id"], name: "index_rooms_on_room_type_id"

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "password"
    t.string   "name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "rol",                    default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
