class CreateBillDetails < ActiveRecord::Migration
  def change
    create_table :bill_details do |t|
      t.references :expend, index: true, foreign_key: true
      t.references :bill, index: true, foreign_key: true
      t.integer :cantidad

      t.timestamps null: false
    end
  end
end
