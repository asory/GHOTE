class CreateRoomTypes < ActiveRecord::Migration
  def change
    create_table :room_types do |t|
      t.string :desc_th
      t.float  :precio

      t.timestamps null: false
    end
  end
end
