class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :cedula
      t.string :nombre
      t.string :apellido
      t.integer :edad
      t.string :direccion
      t.string :telefono

      t.timestamps null: false
    end
  end
end
