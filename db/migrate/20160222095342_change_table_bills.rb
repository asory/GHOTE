class ChangeTableBills < ActiveRecord::Migration
  def up
    remove_column :bills, :references
    remove_column :bills, :cliente
    remove_column :bills, :nro_reserva
  end

  def down
    add_column :bills, :codigo, :string
  end
end
