class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :tipo
      t.string :piso
      t.string :estatus

      t.timestamps null: false
    end
  end
end
