class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.string :nro_reserva
      t.string :cliente
      t.string :references
      t.references :client, index: true, foreign_key: true
      t.references :reservation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
