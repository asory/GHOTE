class CreateReserves < ActiveRecord::Migration
  def change
    create_table :reserves do |t|
      t.date :fecha_fin 
      t.date :fecha_init
      t.string :estatus
      t.float :monto
      t.references :client, index: true, foreign_key: true
      t.references :room, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
