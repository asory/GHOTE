class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string :rif
      t.string :nombre
      t.string :direccion
      t.string :ciudad
      t.string :estado
      t.string :telefono
      t.string :tipo

      t.timestamps null: false
    end
  end
end
