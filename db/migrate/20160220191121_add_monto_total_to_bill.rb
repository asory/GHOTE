class AddMontoTotalToBill < ActiveRecord::Migration
  def change
    add_column :bills, :monto_total, :float
  end
end
