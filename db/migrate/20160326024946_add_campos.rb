class AddCampos < ActiveRecord::Migration
  def change
    add_column :bills, :tipo_pago, :string
    rename_column :rooms, :tipo, :nro_hab
    remove_column :rooms, :descripcion
    remove_column :hotels, :estado
    remove_column :hotels, :ciudad
  end
end
