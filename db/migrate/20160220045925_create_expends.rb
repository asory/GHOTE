class CreateExpends < ActiveRecord::Migration
  def change
    create_table :expends do |t|
      t.string :descripcion
      t.float :monto

      t.timestamps null: false
    end
  end
end
