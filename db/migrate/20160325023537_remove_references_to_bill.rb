class RemoveReferencesToBill < ActiveRecord::Migration
  def change
    remove_reference :bills, :reservation 
    add_reference :bills, :reserve, index: true
  end
end
