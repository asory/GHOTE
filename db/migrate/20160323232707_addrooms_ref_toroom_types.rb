class AddroomsRefToroomTypes < ActiveRecord::Migration
  def change
    add_reference :rooms, :room_type, index: true
    add_reference :hotels, :user, index: true
    add_reference :hotels, :room, index: true
  end
end
